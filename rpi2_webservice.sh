#!/bin/bash

PRODUCT_NAME="rpi2_webservice"
LOCAL_REPO_ROOT=`pwd`"/${PRODUCT_NAME}_repo"
LOCAL_REPO="${LOCAL_REPO_ROOT}/${PRODUCT_NAME}"
REMOTE_REPO_ROOT="https://gitlab.com/anand.s"
REMOTE_MANIFEST_REPONAME="defaultmanifest.git"
REMOTE_MANIFEST_BRANCH="master"
REMOTE_MANIFEST_NAME="default.xml"
CURRENT_DIR=`pwd`

if [ -d "${LOCAL_REPO_ROOT}" ]; then
  echo "Directory ${LOCAL_REPO_ROOT} already exists"
else
  mkdir ${LOCAL_REPO_ROOT}
  cd ${LOCAL_REPO_ROOT}
  repo init -u "${REMOTE_REPO_ROOT}/${REMOTE_MANIFEST_REPONAME}" \
          --manifest-branch="${REMOTE_MANIFEST_BRANCH}" \
          --manifest-name="${REMOTE_MANIFEST_NAME}"

  repo sync

  cd ${LOCAL_REPO}
  TEMPLATECONF=meta-webservice/conf source ${LOCAL_REPO}/oe-init-build-env "build_${PRODUCT_NAME}"
fi

cd ${LOCAL_REPO}
source ${LOCAL_REPO}/oe-init-build-env "build_${PRODUCT_NAME}"


bitbake rpi-webservice-image

RPI_UI_IMAGE_LOCATION=${LOCAL_REPO}/"build_${PRODUCT_NAME}/tmp/deploy/images/raspberrypi2" 
if [ ! -d  ${RPI_UI_IMAGE_LOCATION} ]; then
   echo "Directory ${RPI_UI_IMAGE_LOCATION} is not exists. Quitting."
   exit -1
fi

SD_NAME=$(echo `lsblk -o name,MODEL | grep "SD/*" | awk '{print $1}'`)
if [ -z ${SD_NAME} ];then
   echo "SD CARD IS NOT CONNECTED. Quitting."
   exit -1

else
   SD_LOCATION="/dev/${SD_NAME}"
   echo "SD CARD LOCATION IS :- ${SD_LOCATION}"

   while true;
   do
     PARTITION_NAME=$(echo `df -h | tail -1 | grep "$(echo ${SD_NAME})" | awk '{print $1}'`)
     if [ -z ${PARTITION_NAME} ]; then
         echo "NO PARTITION IS MOUNTED"
         break 
     fi
     echo "${SD_LOCATION}'s PARTITION NAME IS ${PARTITION_NAME}"
     UMOUNT_COMMAND="sudo umount ${PARTITION_NAME}"
     echo "PARTITION - ${PARTITION_NAME} IS UNMOUNTED"
     ${UMOUNT_COMMAND}
   done

   IMAGE_NAME="rpi-ui-image-raspberrypi2.rpi-sdimg"
   IMAGE_INPUT_LOCATION=${RPI_UI_IMAGE_LOCATION}/${IMAGE_NAME}
   #echo ${IMAGE_INPUT_LOCATION}

   FLASH_COMMAND=" sudo dd if=${IMAGE_INPUT_LOCATION} of=${SD_LOCATION}"
   echo ${FLASH_COMMAND}
   ${FLASH_COMMAND}
fi

cd ${CURRENT_DIR}
$SHELL