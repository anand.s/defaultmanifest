Steps to build rpi2 image file
===============================

1. git clone https://gitlab.com/anand.s/defaultmanifest.git
2. cd defaultmanifest
3. sh rpi2_webservice.sh

It will start installing dependencies


Dependencies for creating a webservice on Raspberrypi 2
=======================================================

**Python**

python-sqlite3 (Python wrapper for sqlite)

python-flask (Web framework)

**Java**

openjdk-8

Tomcat-v7.0

**Database**

sqlite3

